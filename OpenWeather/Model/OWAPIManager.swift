//
//  OWAPIManager.swift
//  OpenWeather
//
//  Created by Victor Das on 18/3/20.
//  Copyright © 2020 Victor Das. All rights reserved.
//

import Foundation
import CoreLocation

protocol OWAPIManagerDelegate {
    func didUpdateWeather(_ weatherManager: OWAPIManager, weather: OWDataModel)
    func didFailWithError(error: Error)
}

struct OWAPIManager {
    let weatherURL = "https://api.openweathermap.org/data/2.5/weather?appid=63d78434aed0f45e04db867110af8256&units=metric"
    
    var delegate: OWAPIManagerDelegate?
    
    func fetchWeather(cityName: String) {
        let urlString = "\(weatherURL)&q=\(cityName)"
        performRequest(with: urlString)
    }
    
    func fetchWeather(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let urlString = "\(weatherURL)&lat=\(latitude)&lon=\(longitude)"
        performRequest(with: urlString)
    }
    
    func performRequest(with urlString: String) {
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    self.delegate?.didFailWithError(error: error!)
                    return
                }
                if let safeData = data {
                    if let weather = self.parseJSON(safeData) {
                        self.delegate?.didUpdateWeather(self, weather: weather)
                    }
                }
            }
            task.resume()
        }
    }
    
    func parseJSON(_ weatherData: Data) -> OWDataModel? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(OWData.self, from: weatherData)
            let id = decodedData.weather[0].id
            let temp = decodedData.main.temp
            let name = decodedData.name
            
            let weather = OWDataModel(conditionId: id, cityName: name, temperature: temp)
            return weather
            
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }
    
    
    
}
