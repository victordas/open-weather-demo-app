//
//  OWData.swift
//  OpenWeather
//
//  Created by Victor Das on 18/3/20.
//  Copyright © 2020 Victor Das. All rights reserved.
//

import Foundation

struct OWData: Codable {
    let name: String
    let main: Main
    let weather: [Weather]
}

struct Main: Codable {
    let temp: Double
}

struct Weather: Codable {
    let description: String
    let id: Int
}
